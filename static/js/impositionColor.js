/**
 * Created by PyCharm.
 * User: Кирилл
 * Date: 19.04.12
 * Time: 16:30
 * To change this template use File | Settings | File Templates.
 */
function impositionColor(mode,alpha,canvas,){


    var blendingModes = {
        normal: function(a, b) {
            return a;
        },

        lighten: function(a, b) {
            return (b > a) ? b : a;
        },

        darken: function(a, b) {
            return (b > a) ? a : b;
        },

        multiply: function(a, b) {
            return (a * b) / 255;
        },

        average: function(a, b) {
            return (a + b) / 2;
        },

        add: function(a, b) {
            return Math.min(255, a + b);
        },

        substract: function(a, b) {
            return (a + b < 255) ? 0 : a + b - 255;
        },

        difference: function(a, b) {
            return Math.abs(a - b);
        },

        negation: function(a, b) {
            return 255 - Math.abs(255 - a - b);
        },

        screen: function(a, b) {
            return 255 - (((255 - a) * (255 - b)) >> 8);
        },

        exclusion: function(a, b) {
            return a + b - 2 * a * b / 255;
        },

        overlay: function(a, b) {
            return b < 128
                ? (2 * a * b / 255)
                : (255 - 2 * (255 - a) * (255 - b) / 255);
        },

        softLight: function(a, b) {
            return b < 128
                ? (2 * ((a >> 1) + 64)) * (b / 255)
                : 255 - (2 * (255 - (( a >> 1) + 64)) * (255 - b) / 255);
        },

        hardLight: function(a, b) {
            return blendingModes.overlay(b, a);
        },

        colorDodge: function(a, b) {
            return b == 255 ? b : Math.min(255, ((a << 8 ) / (255 - b)));
        },

        colorBurn: function(a, b) {
            return b == 0 ? b : Math.max(0, (255 - ((255 - a) << 8 ) / b));
        },

        linearDodge: function(a, b) {
            return blendingModes.add(a, b);
        },

        linearBurn: function(a, b) {
            return blendingModes.substract(a, b);
        },

        linearLight: function(a, b) {
            return b < 128
                ? blendingModes.linearBurn(a, 2 * b)
                : blendingModes.linearDodge(a, (2 * (b - 128)));
        },

        vividLight: function(a, b) {
            return b < 128
                ? blendingModes.colorBurn(a, 2 * b)
                : blendingModes.colorDodge(a, (2 * (b - 128)));
        },

        pinLight: function(a, b) {
            return b < 128
                ? blendingModes.darken(a, 2 * b)
                : blendingModes.lighten(a, (2 * (b - 128)));
        },

        hardMix: function(a, b) {
            return blendingModes.vividLight(a, b) < 128 ? 0 : 255;
        },

        reflect: function(a, b) {
            return b == 255 ? b : Math.min(255, (a * a / (255 - b)));
        },

        glow: function(a, b) {
            return blendingModes.reflect(b, a);
        },

        phoenix: function(a, b) {
            return Math.min(a, b) - Math.max(a, b) + 255;
        }
    };

    /** @type CanvasRenderingContext2D */
    var canvas = null;
    var ctx = null;

    /** Current blending mode */
    var mode = mode;

    /** Current blending opacity */
    var alpha = 1;

    var totalImages = 2;
    var img1 = new Image;
    var img2 = new Image;


    img1.onload = imageReady;
    img2.onload = imageReady;
    function imageReady() {
        if (--totalImages == 0) {
            setupScene();
        }
    }

    function setupScene() {

        canvas = document.createElement('canvas');
        ctx=canvas.getContext('2d');
        //console.log(canvas)
        //console.log(ctx)
        drawImage(img1, ctx);
    }

    /**
     * Draw image on specified canvas context
     * @param {Image} img
     * @param {CanvasRenderingContext2D} ctx
     */
    function drawImage(img, ctx) {
        ctx.canvas.width = img.width;
        ctx.canvas.height = img.height;

        ctx.drawImage(img, 0, 0);
        updateScene();
    }

    function updateScene() {
        var imageData1 = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);

        /** @type Array */
        var pixels1 = imageData1.data;


        var r, g, b, oR, oG, oB, alpha1 = 1 - alpha;
        var blendingMode = blendingModes[mode];

        var whileLn = ctx.canvas.width*3;
        var colorLn = 255;
        var SwigLn = 255/(pixels1.length/whileLn);
        var nbr =0 ;



        // blend images
        for (var i = 0, il = pixels1.length; i < il; i += 4) {
            oR = pixels1[i];
            oG = pixels1[i + 1];
            oB = pixels1[i + 2];

            // calculate blended color
            r = blendingMode(colorLn, oR);
            g = blendingMode(colorLn, oG);
            b = blendingMode(colorLn, oB);

            // alpha compositing
            pixels1[i] =     r * alpha + oR * alpha1;
            pixels1[i + 1] = g * alpha + oG * alpha1;
            pixels1[i + 2] = b * alpha + oB * alpha1;


            if (i / whileLn% 1  == 0){
                colorLn=colorLn-SwigLn;
            }
        }


        ctx.putImageData(imageData1, 0, 0);
        $('.header').css({'background-image':"url(" + canvas.toDataURL("image/png")+ ")" });

    }

    /**
     * @param {String} name
     */
    function humanizeName(name) {
        return name.charAt(0).toUpperCase() + name.substring(1).replace(/[A-Z]/g, function(s) {
            return ' ' + s.toLowerCase();
        });
    }
}