$(document).ready(function() // После загрузки страницы
{
	if(window.location.hash)
		RequestModifcationFilter(window.location.hash.substr(1));
	else
		OnLoadModificationPage();
});

function RequestModifcationFilter(params) //Функция для запроса занных модификаций по фильтру через Ajax
{
	if(!params) params = "";
	$.ajax({
		data: params,
		dataType: 'json',
		type: "POST",
		success: OnSuccessFilter,
		url: 'http://'+window.location.host+window.location.pathname
	});
}

function OnLoadModificationPage() //при загрузке страницы
{
	$("a[rel=modification-filter]").click(function(){
		url = $(this).attr('href').split("?")
		hash = (url[1])?url[1]:'';
		window.location.hash = hash;
		RequestModifcationFilter(hash)
		return false;
	})
}

function OnSuccessFilter(data, textStatus, jqXHR) //При получении списка модификаций
{
	$("#filter_list").html(data["filter"]);
	$("#modifications_list").html(data["modifications"]);
	OnLoadModificationPage();
}