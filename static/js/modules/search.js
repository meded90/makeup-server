/**
* Обработчики событий
*/

function OnFilterManufacturerChange()
{
    $("#search_action").val('manufacturer');
    while($("#actions > div").length>1)
    {
        $("#actions > div").eq(1).remove()
    }
    $(this).parents("form").submit();
    $("#search_action").val('ignore');
}

function OnFilterManufacturerKeyDown(event)
{
    if((event.which == 38) || (event.which == 40))
    {
        OnFilterManufacturerChange()
    }
}

function OnFilterModelChange()
{
    $("#search_action").val('model');
    while($("#actions > div").length>2)
    {
        $("#actions > div").eq(2).remove()
    }
    $(this).parents("form").submit();
    $("#search_action").val('ignore');
}

function OnFilterModelKeyDown(event)
{
    if((event.which == 38) || (event.which == 40))
    {
        OnFilterModelChange()
    }
}

function OnFilterDateChange()
{
    $("#search_action").val('date');
    while($("#actions > div").length>3)
    {
        $("#actions > div").eq(3).remove()
    }
    $(this).parents("form").submit();
    $("#search_action").val('ignore');
}

function OnFilterDateKeyDown(event)
{
    if((event.which == 38) || (event.which == 40))
    {
            OnFilterDateChange()
    }
}

function OnFilterAttrChange()
{
	$("#search_action").val('details');
	$("#search_result").html('');
}

function OnFilterAttrKeyDown(event)
{
    if((event.which == 38) || (event.which == 40))
    {
            $("#search_action").val('details');
            $("#search_result").html('');
    }
    if(event.which == 13)
    {
        $(this).parents("form").submit();
        $("#search_action").val('ignore');
    }
}

function OnSearchAjaxFormSubmit()
{
    var mode = $("#search_action").val();
    //console.log('StartMode:'+mode)
    $("#search_action").val('ignore');
    //console.log($("#search_action").val())

    $(this).find('#silent-input').blur();
    form = $(this);
        if('manufacturer' == mode)
	{
            $.get('/search/manufacturer/'+$(this).find("#manufacturer_select").val()+ '/' + $('#silent-input').val(), function(data){
                submitBtn = $("#search-submit");

                for(key in data.models)
                        length++;
                if(length)
                {
                    var select = $("<select>").attr('id','model_select').attr('name','model_select').change(OnFilterModelChange).keydown(OnFilterModelKeyDown);
                    for(key in data.models)
                            $("<option>").val(data.models[key]).html(data.models[key]).appendTo(select);


                    var div = $('<div>');
                    var text = "Выберите модель вашего автомобиля:";
                    div.html(text);
                    div.append(select);
                    $("#actions").append(div);
                    $("#search_action").val('model');
                    log($("#search_action").val());

                if(data.model)
                {
                    if(data.model in data.models)
                        return;
                    select.val(data.model);
                    //console.log(data.model);
                    $("#suggestion_box").append($("<p>").html("В вашем запросе было найдено ключевое слово. Модель: "+data.model+"."));
                    $("#filter_main").submit();
                }
			}
            else
            {
                $("#search_result").html('<h2>Детали</h2><p>Деталь не найдена</p>')
                $("#search_action").val('query');
            }
		}, "json")
	}
	else if('model' == mode)
	{
            $.get('/search/model/'+$(this).find("#model_select").val().replace(/\//g,'^')+ '/' + $('#silent-input').val(), function(data){
                submitBtn = $("#search-submit");

                if(data.startDate)
                {
                    var select = $("<select>").attr('id','out_date').attr('name','out_date').change(OnFilterDateChange).keydown(OnFilterDateKeyDown);
                    for(i=data.startDate;i<=data.endDate;i++)
                            $("<option>").val(i).html(i).appendTo(select);
                        
                    var div = $('<div>');
                    var text = "Введите год выпуска вашего автомобиля:";
                    div.html(text);
                    div.append(select);
                    $("#actions").append(div);

                    $("#search_action").val('date');

                    if(data.year)
                    {
                        select.val(data.year);
                        $("#suggestion_box").append($("<p>").html("В вашем запросе было найдено ключевое слово. Год: "+data.year+"."));
                        $("#filter_main").submit();
                    }

                }
            else
            {
                $("#search_result").html('<h2>Детали</h2><p>Деталь не найдена</p>')
                $("#search_action").val('query');
            }
		}, "json")
	}
	else if('date' == mode)
	{

            $.get('/search/date/'+$(this).find("#model_select").val().replace(/\//g,'^')+ '/'+$(this).find("#out_date").val()+'/' + $('#silent-input').val(), function(data){
                    var translations = data["attrsTranslation"];
                    data = data["attributes"];

                length = 0;
                for(key in data)
                    length++;

                if(length)
                {
                    text = "<h2>Выбирите аттрибуты</h2>";
                    tempDiv = $("<div>");

                    for(var key in data)
                    {
                        var item = $("<div>").html(translations[key]);

                        var select = $("<select>").attr('name','param['+key+']').change(OnFilterAttrChange).keydown(OnFilterAttrKeyDown);
                        $("<option>").val(0).html('Любой').appendTo(select)
                        for(var attrValue in data[key])
                                $("<option>").val(attrValue).html(data[key][attrValue]).appendTo(select)
                        item.append(select);
                        tempDiv.append(item)
                    }
                    var div = $("<div>");
                    div.html(text);
                    div.append(tempDiv);
                    $("#actions").append(div);

                    $("#search_action").val('details');
                }
                else
                    $("#search_result").html('<h2>Детали</h2><p>Деталь не найдена</p>')
            }, "json")
	}
    else if('details' == mode)
	{
		//console.log('/search/date/'+$(this).find("#model_select").val().replace(/\//g,'^')+ '/'+$("#out_date").val()+'/' + $('#silent-input').val());
		$.get('/search/date/'+$(this).find("#model_select").val().replace(/\//g,'^')+ '/'+$(this).find("#out_date").val()+'/' + $('#silent-input').val(), $(this).serialize(), function(data){
			data = $.parseJSON(data);
			submitBtn = $("#search-submit");
			$("#search_result").html(data.details)
		})
	}
	return false;
}

$(document).ready(function() // После загрузки страницы
{
    $("#manufacturer_select").change(OnFilterManufacturerChange).keydown(OnFilterManufacturerKeyDown);
    $("#filter_main").submit(OnSearchAjaxFormSubmit);
    //$("#filter_main").find('#search-input').keydown(OnFilterDetailKeyDown);
})