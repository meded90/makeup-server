$(document).ready(function(){
	$('.brands-box__inner').ready(function(){
		var $brandBox = $('.brands__inner');
		var $brands = $('.brands-box__inner > ul > li');
		
		var brandCount = $('.brands-box__inner > ul > li:gt(6):not(.more-brands)').length;
		
		$('.brands-box__inner > ul > li:gt(6):not(.more-brands)')
			.addClass('add-to-popover')
			.prependTo( '.more-brands .popover-content ul' );
		
		
		if ( brandCount == 0) {
			$('.more-brands').hide();
		}
		$brandBox.css({
			'margin-left': - $brandBox.width() / 2,
			'left': '50%'
		});
	})
});