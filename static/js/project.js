
isDebug=true;
function log(k)
{
    if(isDebug)
        console.log(k)
}
var is=0;
$(document).ready(function(){

    // выравнивание лого в шапке по вертикали
    $.fn.vAlign = function() {
    return this.each(function(i){
        var ah = $(this).height();
        var ph = $(this).parent().height();      
        var mh = Math.ceil((ph-ah) / 2); //var mh = (ph - ah) / 2;
        $(this).css('padding-top', mh);
      });
    };

    $(".nested-img").vAlign();

    window.onload = function() {
        $(".main-logo").vAlign();
    };

    $(".spinner").spinner();

    $(".phone-mask").mask("999-999-99-99",{placeholder:" "});

    $(".helper__quotes").click(function () {
        $(this).parents(".helper").toggleClass('open')
    });

    $('input, textarea').placeholder();

    $('.more-info').not('[class~="top-menu__more-info"]').popover({ title:"Справка.", trigger: 'click' });

    $(".popover.hide.fade").modal({
        "show":false,
        "backdrop":false
    });

    $('form').submit(function()
    {
        // Блокируем кнопки при отправке формы
        $('input[type=submit]', this).attr('disabled', 'disabled');
    });
    $('.mangerPanel_button').click(function(){
        var myDate = 3600*24*365;
        var curDate = new Date();
        //console.log(myDate);
        if ($(this).hasClass('active'))
        {
            $.cookie('managerTopPanel', true, {expire: 7});
            $(this).removeClass('active');
            $('.top-ponel__collapse-link-text').text('Отобразить панель');
        }
        else
        {
            $.cookie('managerTopPanel', false, {expires: 7});
            $(this).addClass('active');
            $('.top-ponel__collapse-link-text').text('Скрыть панель');
        }
    });

//    $('.resize_auto').autoResize({
//        // On resize:
//        onResize : function() {
//            $(this).css({opacity:0.8});
//        },
//        // After resize:
//        animateCallback : function() {
//            $(this).css({opacity:1});
//        },
//        // Quite slow animation:
//        animateDuration : 300,
//        // More extra space:
//        extraSpace : 40
//    });
    //обявления
    $(".ask-question-bar.hidden_form h2").click(function(){
    	if ( $(this).parents(".ask-question-bar").hasClass('hidden_form') ) {
	       $(this).parents(".ask-question-bar").removeClass("hidden_form");
	       $(this).parent().next().show("fast");
    	} else {
	       $(this).parents(".ask-question-bar").addClass("hidden_form");
	       $(this).parent().next().hide("fast");
    	}
    	return false;
    });

    $('.resultat').button()
    // карусель

    var clickSpeed = 500; // the speed of animation when clicking a nav button
    var autoSpeed = 500; // when autoscrolling to right
    var repeatSpeed = 6000; // time when to repeat the autoScroll
    var moveflag = 0;
    // Non editable vars
    var scroll;
    var timerID = null;

    // Set the left css style - so that we have the first element hidden on the left
    // using setTimeout - for Chrome
    setTimeout(function(){
        var w = $('#carousel_ul li:first').outerWidth() + 10;
        $('#carousel_ul').css({'left' : w * -1});
    },1);

    //when user clicks the image for sliding right
    $('#right_scroll').click(scrollRigth);
    

    //when user clicks the image for sliding left
    $('#left_scroll').click(ScrollLeft);
    
    function ScrollLeft(){
        clearInterval (timerID); // clear the interval - disable autoscrolling
        disableNavButtons(); // disable Nav Buttons when scrolling is happening

        var item_width = $('#carousel_ul li').outerWidth() + 10;

        // same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width)
        var left_indent = parseInt($('#carousel_ul').css('left')) + item_width;
        var $last = $('#carousel_ul li:last');

        $('#carousel_ul:not(:animated)').animate({'left' : left_indent},clickSpeed,function(){
            var width = $last.outerWidth() + 10;
            $('#carousel_ul li:first').before( $last);
            $last.click(FillSearchInput);
            $('#carousel_ul').css({'left' : width * -1});

            enableNavButtons(); // Enable Nav Buttons back
            timerID = null;
            startAutoScroll(); // reset timer
        });
    }
    
    function scrollRigth(){
        clearInterval (timerID); // clear the interval - disable autoscrolling
        disableNavButtons(); // disable Nav Buttons when scrolling is happening

        var item_width = $('#carousel_ul li').outerWidth() + 10;

        // same as for sliding right except that it's current left indent + the item width (for the sliding right it's - item_width)
        var left_indent = parseInt($('#carousel_ul').css('left')) - item_width;
        var $first = $('#carousel_ul li:first');

        $('#carousel_ul:not(:animated)').animate({'left' : left_indent}, clickSpeed, function(){
            var width = $first.outerWidth() + 10;
            $('#carousel_ul li:last').after( $first);
            $('#carousel_ul').css({'left' : left_indent+width });
            

            enableNavButtons(); // Enable Nav Buttons back
            timerID = null;
            startAutoScroll(); // reset timer
        });
    }

    

    function enableNavButtons()
    {
        $('#right_scroll').click(scrollRigth);
        $('#left_scroll').click(ScrollLeft);
        $('#right_scroll img').removeAttr('disabled');
        $('#left_scroll img').removeAttr('disabled');
    }

    function disableNavButtons()
    {
        $('#right_scroll').unbind("click");
        $('#left_scroll').unbind("click");
        $('#right_scroll img').attr('disabled', true);
        $('#left_scroll img').attr('disabled', true);
    }

    function startAutoScroll()
    {
        if (timerID === null) { // to avoid multiple registration
            timerID = setInterval (function(){scrollRigth(autoSpeed);}, repeatSpeed);
        }
    }
    startAutoScroll();



    // Расрываюшиеся дерево
    $(".tree_branch").click(function(){
        $(this).parent().children(".active").removeClass("active") ;
        $(this).parent().children(".active").find(".active").removeClass("active");
        $(this).addClass("active").slideDown() ;
        return false;
    });
    // slider
    $( "#slider" ).slider();
//всплывающме окна
    hideEvent = null;
    $('.popup-onhover-show').hover(function(){
        var offset = $(this).offset();
        var x = offset.left;
        var y = offset.top;
        var withObject = $(this).width();
        var popupObject = '#' + $(this).attr('rel');
        var withpopupObject = $(popupObject).width();
        var margenObject = -(withpopupObject)/2;
        //console.log(withpopupObject)
        $(popupObject ).show(100).css('marginLeft',margenObject).css('top',y ).css('left',x+(withObject/2));
        clearTimeout(hideEvent);
    }, function(){
        hideEvent = setTimeout("$('.popup-onhover').hide();", 300);
    });

    $('.popup-onhover').hover(function(){
        clearTimeout(hideEvent);
    }, function(){
        hideEvent = setTimeout("$('.popup-onhover').hide();", 300);
    })
        //оформление селектора
    $('.selectBlock').sSelect();


    $('.accordion').ready(function(){
        $('.accordion').each(function(i, obj){
            $(obj).find('.accordion_content').hide();


            $(obj).find('.accordion_heading').click(function(){
                if (!($(this).find('input[type=radio]').attr('checked'))){

                    $(obj).find('.accordion_content').slideUp(300);
                    $(obj).find('.accordion_heading').addClass("cursor");
                    $(obj).find('.heading').addClass("link_in");

                    $(this).next().slideDown(300);
                    $(this).find('input[type=radio]').attr('checked', true);
                    $(this).find('.heading').removeClass("link_in");
                    $(this).removeClass("cursor");

                }

            });
            $(obj).find('input[type=radio]').change(function(){
                if (!$( 'obj input[type=radio]').attr('checked')){

                    $(obj).find('.accordion_content').slideUp(300);
                    $(obj).find('.accordion_heading').addClass("cursor");
                    $(obj).find('.heading').addClass("link_in");

                    $(this).parent().next().slideDown(3000).fadeIn(300);

                    $(this).parent().find('.heading').removeClass("link_in");
                    $(this).parent().removeClass("cursor");

                }
            });
        });
    });

    $('.accordion').find('.accordion_heading:first').click();
    $('.accordion').find('.accordion_heading:first ').next().show(0);

    //личный кабинет
    $(".informationMini").click(function(){
        $(this).toggleClass("active");
        $(this).next(".informationAll").toggle();
        //$(this).next(".informationAll").find("table").toggle();//hak ie7
    });

    $(".price-quan").keypress(function(event){
        if ((event.which < 48 || event.which > 57) && event.which != 190 && event.which != 8)
        {
            event.preventDefault();
        }
    });
    $(".price-quan").keyup(function(event){

        var quan=$(this).val();
        var filterQuan = "";
        for(i=0; i < quan.length; i++)
            if(quan[i] in ["0","1","2","3","4","5","6","7","8","9"])
                filterQuan += quan[i];
        if(quan != filterQuan)
        {
            quan = filterQuan;
            $(this).val(quan);
        }


        var id = $(this).get(0).id.substring(6);
        var cost=$("#cost-"+id).html();
        var newcost=quan*cost;
        $("#summ-"+id).html(newcost.toFixed(2));//цена в зависимости от кол-ва
        var summ = 0
        $('.cost').each(function() {
            summ+= parseFloat($(this).html());
        });


        $("#finalsum").html(summ.toFixed(2))//итоговая цена
        $("#prepayment").html((summ*0.7).toFixed(2))//предоплата 70%
    });

    $('.map').click(function(){//всплывающая карта схемы проезда
        $('#myMap').click();
    });

	if ( $.mask !== undefined ) {
    	$(".phone-mask").mask("+7 (999) 999-99-99");
	}

    $(".update-captcha").click(function(){

        log("updating")
        $(".captcha").attr("src", "/captcha?"+Math.random());
        return false;
    });

    $('[rel=tooltip],[data-tooltip],').tooltip();
    
    /**
     * Рейтинги: звездочки наши. Тут анимация и обработка клика. Хэндлер тоже писать сюда
     */
	$('.rate:not(.static)').hover(function(){
		$('.rate li').hover(function(){
			if ( $(this).parent().attr('rel') !== undefined ) {
				$(this).parent().find('li').removeClass('a');
				$(this).parent().find('li:lt(' + $(this).index() + ')').addClass('tmp_active');
				$(this).addClass('tmp_active');
			}
		}, function(){
			$(this).removeClass('tmp_active');
		});
	}, function(){
		var index = parseInt($(this).attr('rel'));
		
		$(this).find('li').removeClass('tmp_active');
		$(this).find('li:lt(' + index + ')').addClass('a');
	});
	$('.rate li').click(function(){
		alert('А тут надо писать аякс для обработка клика');
	});
    
    // активы для кнопок кторе вызвают мобалное коно
    $(".helper__link").click(function () {
        $(this).addClass("active")
    });
    $("#informModal").on('hidden', function () {
        $(".helper__link").removeClass("active");
    });
    $(".top-ponel__notice > a").click(function () {
        $(this).addClass("active")
    });
    $("#noticeModal").on('hidden', function () {
        $(".top-ponel__notice > a").removeClass("active");
    });

    $('.modal').on('show', function(){
        $(this).css('top', $(window).scrollTop() + 100);
    });
});

function FillSearchInput()
{
    //  console.log($(this));
    $('#search-input').val($.trim($(this).text()));
    $('#search-input').focus();

    return false;
}