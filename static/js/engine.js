isDebug=true;
$(document).ready(function() // После загрузки страницы
{
	document.ajaxStack = 0;
    document.ajaxModal = true;
	$('#search-form-step1').submit(OnSearchStep1FormSubmit);

	$(document).ajaxSend(function(){
		document.ajaxStack++;
		AjaxRequestDoing();
	});

	$(document).ajaxComplete(function(){
		document.ajaxStack--;
		if(!document.ajaxStack)
			AjaxRequestDestroy();
	})

	$(document).ajaxError(function(){
		alert('We have an error in AJAX query')
	});
    
    $(".reqConfirm").click(function(){
        return confirm("Вы уверены?")        
    })

});

/**
 *Глобальные Ajax события
 */
function AjaxRequestDoing()  //Инициализация Ajax запроса(блокировка страницы)
{
	var topBox, contentBox;
    if(!document.ajaxModal) return;
	if ($("#topLayout").length > 0)
		topBox = $("#topLayout").eq(0)
	else
	{
		topBox = jQuery('<div>', {
			id: 'topLayout',
			css:
			{
				'position':'absolute',
				'top': 0,
				'left': 0,
				'height': $('body').height(),
				'width': $(window).width(),
				'background': 'url(/web/img/layoutBG.png)',
				'z-index': 100
			}
		}).appendTo('html');
		contentBox = jQuery('<div>', {
			id: 'topLayoutContent',
			css:
			{
				'position':'absolute',
				'padding': '10px',
				'text-align:': 'center',
				'border': '#dddddd solid 3px',
				'width': 450,
				'background': '#ffffff',
				'z-index': 101
			},
			html: '<img src="/web/img/loading.gif" style="float:left; margin-right:10px;" /> <span style="font-size:18px;">Подождите! Идет загрузка содержимого!</span>'
		}).appendTo(topBox);
	}

	$("#topLayout").css({
		'height': $('body').height(),
		'width': $(window).width()
	})

	$("#topLayoutContent").css({
		'top': ($(window).height() - $("#topLayoutContent").height()) / 2+$(window).scrollTop() + "px",
		'left': ( $(window).width() - $("#topLayoutContent").width() ) / 2+$(window).scrollLeft() + "px"
	})

	$("#topLayout").show();

	$(window).scroll(function () {
	$("#topLayoutContent").css({
			'top': ($(window).height() - $("#topLayoutContent").height()) / 2+$(window).scrollTop() + "px",
			'left': ( $(window).width() - $("#topLayoutContent").width() ) / 2+$(window).scrollLeft() + "px"
		})
	})
}
function AjaxRequestDestroy() //Завершение получение данных Ajax запросом
{
	$(window).unbind('scroll');
	$("#topLayout").hide();
}

function OnSearchStep1FormSubmit() //При нажатии на кнопку поиска
{
	window.location = '/search/query/' + $(this).find('#search-input').val();
	return false;
}

function log(k)
{
    if(isDebug)
        console.log(k)
}